//
//  TransitionHandler.swift
//  MultidirectionalTransition
//
//  Created by Victor Magalhaes on 07/07/2018.
//  Copyright © 2018 Victor Magalhaes. All rights reserved.
//

import UIKit

class TransitionInteractionHandler: UIPercentDrivenInteractiveTransition {
    
    var transitionInProgress = false
    
    private var shouldCompletePresentTransition = false
    private var shouldCompleteDismissTransition = false
    
    private var fromViewController: UIViewController!
    private var toViewController: UIViewController!
    
    init(from: UIViewController, to: UIViewController) {
        super.init()
        self.fromViewController = from
        self.toViewController = to
        setupGesture(in: fromViewController.view, and: toViewController.view)
    }
    
    private func setupGesture(in fromView: UIView, and toView: UIView) {
        fromView.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(presentGesture(_:))))
        toView.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(dismissGesture(_:))))
    }
    
    @objc func presentGesture(_ gestureRecognizer: UIPanGestureRecognizer) {
        guard let view = gestureRecognizer.view?.superview else { return }
        let translation = gestureRecognizer.translation(in: view)
        var progress = (translation.y / view.frame.height)
        progress = CGFloat(fminf(fmaxf(Float(progress), 0.0), 1.0))

        switch gestureRecognizer.state {
        case .began:
            transitionInProgress = true
            if progress > 0 {
                self.fromViewController.present(self.toViewController, animated: true, completion: { [weak self] in
                    guard let `self` = self else { return }
                    self.shouldCompletePresentTransition = false
                })
            }
        case .changed:
                shouldCompletePresentTransition = progress > 0.3
                update(progress)
        case .cancelled:
            transitionInProgress = false
            cancel()
        case .ended:
            transitionInProgress = false
            shouldCompletePresentTransition ? finish() : cancel()
        default:
            break
        }
    }
    
    @objc func dismissGesture(_ gestureRecognizer: UIPanGestureRecognizer) {
        guard let view = gestureRecognizer.view?.superview else { return }
        let translation = gestureRecognizer.translation(in: view)
        var progress = -(translation.y / view.frame.height)
        print(progress)
        progress = CGFloat(fminf(fmaxf(Float(progress), 0.0), 1.0))
        
        switch gestureRecognizer.state {
        case .began:
            transitionInProgress = true
            self.toViewController.dismiss(animated: true, completion: { [weak self] in
                guard let `self` = self else { return }
                self.shouldCompleteDismissTransition = false
            })
        case .changed:
            shouldCompleteDismissTransition = progress > 0.3
            update(progress)
        case .cancelled:
            transitionInProgress = false
            cancel()
        case .ended:
            transitionInProgress = false
            shouldCompleteDismissTransition ? finish() : cancel()
        default:
            break
        }
    }
}
