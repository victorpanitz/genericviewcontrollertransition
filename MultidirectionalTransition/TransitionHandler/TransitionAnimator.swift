//
//  PresentTransition.swift
//  MultidirectionalTransition
//
//  Created by Victor Magalhaes on 07/07/2018.
//  Copyright © 2018 Victor Magalhaes. All rights reserved.
//

import Foundation
import UIKit

class TransitionAnimator: NSObject, UIViewControllerAnimatedTransitioning, UIViewControllerTransitioningDelegate {
    
    let duration: TimeInterval = 1.0
    private var isPresenting = true
    
    let interactionController: TransitionInteractionHandler?

    init(_ interactionHandler: TransitionInteractionHandler) {
        self.interactionController = interactionHandler
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return duration
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let fromView = transitionContext.view(forKey: .from),
            let toView = transitionContext.view(forKey: .to)
            else { return }
        
        let container = transitionContext.containerView
        
        isPresenting ?
            { container.addSubview(toView); container.addSubview(fromView) }() :
            { container.addSubview(fromView); container.addSubview(toView) }()
        
        isPresenting ?
            toView.transform = CGAffineTransform(translationX: 0, y: -container.frame.height) :
            (toView.transform = CGAffineTransform(translationX: 0, y: container.frame.height))
                    
        UIView.animate(withDuration: duration, animations: { [weak self] in
            guard let `self` = self else { return }
            toView.transform = CGAffineTransform.identity
            self.isPresenting ?
                fromView.transform = CGAffineTransform(translationX: 0, y: container.frame.height) :
                (fromView.transform = CGAffineTransform(translationX: 0, y: -container.frame.height))
        }) { (_) in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        self.isPresenting = true
        return self
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        self.isPresenting = false
        return self
    }
    
    func interactionControllerForPresentation(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        guard let animator = animator as? TransitionAnimator,
            let controller = animator.interactionController,
            controller.transitionInProgress
            else {
                return nil
        }
        isPresenting = true
        return controller
    }
    
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        guard let animator = animator as? TransitionAnimator,
            let controller = animator.interactionController,
            controller.transitionInProgress
            else {
                return nil
        }
        isPresenting = false
        return controller
    }
    
    
}
