//
//  RightViewController.swift
//  MultidirectionalTransition
//
//  Created by Victor Magalhaes on 07/07/2018.
//  Copyright © 2018 Victor Magalhaes. All rights reserved.
//

import UIKit

class TopViewController: UIViewController {
        
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.view.backgroundColor = UIColor.white
        let mView = UIView(frame: CGRect(
            x: 0,
            y: self.view.frame.height - 50,
            width: self.view.frame.width,
            height: 50))
        mView.backgroundColor = .orange
        self.view.addSubview(mView)        
    }

}


