//
//  ViewController.swift
//  MultidirectionalTransition
//
//  Created by Victor Magalhaes on 07/07/2018.
//  Copyright © 2018 Victor Magalhaes. All rights reserved.
//

import UIKit

class CenterViewController: UIViewController {

    var transitionHandler: TransitionInteractionHandler?
    var transitionAnimator: TransitionAnimator?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let mView = UIView(frame: CGRect(
            x: 0,
            y: 0,
            width: self.view.frame.width,
            height: 50))
        mView.backgroundColor = .blue
        self.view.addSubview(mView)
        view.backgroundColor = .white
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        setupTransition()
    }
    
    private func setupTransition() {
        let viewController = TopViewController()
        transitionHandler = TransitionInteractionHandler(from: self, to: viewController)
        transitionAnimator = TransitionAnimator(transitionHandler!)
        viewController.transitioningDelegate = transitionAnimator
    }
    
}
